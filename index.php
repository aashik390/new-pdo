<?php           
$link = $_SERVER['REQUEST_URI'];
$link_array = explode("/",$link);
if (is_file('config.php')) {
  if ($link_array[2] == "index.php") {
    if ($link_array[3] == "login") { 
          require_once 'model/model.php';
          $conn = db_get_connection();
          session_start();
            if(isset($_POST['login']))
            {
              $uname=$_POST['username'];
              $password=md5($_POST['password']);
              $sq = "SELECT username,password,email from users WHERE username = '$uname' and password = '$password'";
              $res = $conn->query($sq);
              $res->setFetchMode(PDO::FETCH_ASSOC);
              $count = $res->rowCount();
            if($count > 0)
            {
              $_SESSION['userlogin']=$_POST['username'];
              echo "<script type='text/javascript'> document.location = 'http://localhost/mob/index.php'; </script>";
            } 
            else{
              echo "<script>alert('Invalid Details');</script>";
            }
          }
      include 'views/login.html';
      die;
    }
    if ($link_array[3] == "logout") {
      session_start();
      unset($_SESSION['userlogin']); 
      session_destroy();
      echo "<script> alert('Log-out successfull!');
                  window.location.href='http://localhost/mob/index.php';
                </script>"; 
    }
    if ($link_array[3] == "signup") {
      require_once 'model/model.php';
      $conn = db_get_connection();
      if(!empty($_POST["username"])) {
        $uname= $_POST["username"];
        $sql = "SELECT username FROM  users WHERE username=:uname";
        $query = $conn -> prepare($sql);
        $query-> bindParam(':uname', $uname, PDO::PARAM_STR);
        $query-> execute();
        $results = $query -> fetchAll(PDO::FETCH_OBJ);
      if($query -> rowCount() > 0)
      {
      echo "<span style='color:red'> Username already exists.</span>";
      }
      }
      if(!empty($_POST["email"])) {
        $email= $_POST["email"];
        $sql ="SELECT email FROM  users WHERE email=:email";
        $query = $conn -> prepare($sql);
        $query-> bindParam(':email', $email, PDO::PARAM_STR);
        $query-> execute();
        $results = $query -> fetchAll(PDO::FETCH_OBJ);
        if($query -> rowCount() > 0)
        {
          echo "<span style='color:red'>Email-id already exists.</span>";
        }
      }
      if(isset($_POST['signup']))
      {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = md5($_POST['password']);
        $password1 = md5($_POST['password_confirm']);
        if ($password !== $password1) {
          echo "<script> alert('Password mismatch');
                  window.location.href='http://localhost/mob/index.php/signup';
                </script>";
          die;      
        }
        $ret = "SELECT username , password , email FROM users where (username=:uname ||  email=:uemail)";
        $queryt = $conn -> prepare($ret);
        $queryt->bindParam(':uemail',$email,PDO::PARAM_STR);
        $queryt->bindParam(':uname',$username,PDO::PARAM_STR);
        $queryt -> execute();
        $results = $queryt -> fetchAll(PDO::FETCH_OBJ);
        if($queryt -> rowCount() == 0)
        {
          $sql = "INSERT INTO users(username,email,password) VALUES(:uname,:uemail,:upassword)";
          $query = $conn->prepare($sql);
          $query->bindParam(':uname',$username,PDO::PARAM_STR);
          $query->bindParam(':uemail',$email,PDO::PARAM_STR);
          $query->bindParam(':upassword',$password,PDO::PARAM_STR);
          $query->execute();
          $lastInsertId = $conn->lastInsertId();
          if($lastInsertId)
          {
            echo "<script> alert('Registration successfull!');
                  window.location.href='http://localhost/mob/index.php/login';
                </script>";
          }
          else
          {
            echo "<script type='text/javascript'> alert('Something went wrong');</script>";
          }
        }
      }
      include 'views/signup.html';
      die;
    }
    if ($link_array[3] == "tag") {  
      require_once 'model/model.php';
      $conn = db_get_connection();
      $tagIdValue = $link_array[4];
      if (isset($_GET['pageno'])) {
        $pageno = $_GET['pageno'];
      } 
      else {
        $pageno = 1;
      }
        $n = 3;
        $total_pages = pageTag($conn, $n, $tagIdValue);
      if ($pageno > $total_pages) {
        $pageno = 1;
      }
      $offset = ($pageno-1) * $n;
      if ($link_array[5]=="ASC") {
        $sort = ASC;
      } 
      else {
        $sort = DESC;
      }
      $d = sortDisplayTag($conn, $offset, $sort, $n, $tagIdValue);
      include 'views/tagpost.php';
    }

    if($link_array[3]=="category"){
      session_start();
      if(strlen($_SESSION['userlogin'])==0)
      {
        echo "<script> alert('You must login first!');
            window.location.href='http://localhost/mob/index.php/login';
            </script>";
      }
      else {
       if($link_array[4]=="edit"){
        require_once 'model/model.php';
        $conn = db_get_connection();
        $cid = $link_array[5] ;
        if (isset($_POST['submit'])) {
          
          $cname = $_POST['cat'];
          updateCategory($cid, $cname, $conn);     }
          $d = fetchAllCategory($conn);
          session_start();
          if(strlen($_SESSION['userlogin'])==0) 
          {
            echo "<script> alert('You must login first!');
            window.location.href='http://localhost/mob/index.php/login';
            </script>";
          }
          else{
          include 'views/edit_cat.php';
          die;
          }   
      }
      if ($link_array[4] == "show" and is_numeric($link_array[5])) {
        require_once 'model/model.php';
        $conn = db_get_connection();
        $catIdValue = $link_array[5];
        if (isset($_GET['pageno'])) {
          $pageno = $_GET['pageno'];
        } 
        else {
          $pageno = 1;
        }
          $n = 5;
          $total_pages = pageTag($conn, $n, $tagIdValue);
        if ($pageno > $total_pages) {
          $pageno = 1;
        }
        $offset = ($pageno-1) * $n;
        if ($link_array[6] == "ASC") {
          $sort = ASC;
        } 
        else {
          $sort = DESC;
        }
        $d = sortDisplaycategory($conn, $offset, $sort, $n, $catIdValue);
        include 'views/category.php';
        die;
        }
        require_once 'model/model.php';
        $conn = db_get_connection();
        if (isset($_POST['submit'])) {
          $cat = $_POST['cat'];
          $count = categoryCount($conn, $cat);
          if ($count == 0) {
            insertCategory($conn, $cat);
          } 
          else {
            echo "<script> alert('Category already exists!');
              window.location.href='cat.php';
              </script>";
          }    
      }       
        try {         
          $id = $link_array[4];
          deleteCategory($conn, $id);
        } catch(Exception $e) {
            echo 'Message: ' .$e->getMessage();
          }
        $d = fetchAllCategory($conn);  
        include 'views/cat.php';
    }
  }

    if($link_array[3] == "add"){ 
      if (isset($_POST['submit'])){
          require_once 'model/model.php';
          $conn = db_get_connection();
              $title = $_POST['title'];
              $blog = $_POST['content'];
              $tag = $_POST['tags'];
              $date = date("Y-m-d H:i:s");
              $words = trimInput($tag);
              $s = sizeof($words); 
              insertBlog($title, $blog, $date, $conn);
              for ($i = 0; $i < $s; $i++) { 
                $count = tagCount($conn, $words, $i);
                if ($count == 0) {
                  insertTag($conn, $words, $i);
                }
                insertRelation($conn, $words, $title, $i);
              }
              $sq = "SELECT id FROM BlogDetails WHERE Title = '$title'";
              $q = $conn->query($sq);
              $q->setFetchMode(PDO::FETCH_ASSOC);
              $id = $q->fetch();
              foreach($_POST['list'] as $s) {
                $sql = "SELECT cid FROM CatTable WHERE cname = '$s'";
                $q = $conn->query($sql);
                $q->setFetchMode(PDO::FETCH_ASSOC);
                $cid = $q->fetch();
                insertCategoryRelation($conn, $id['id'], $cid['cid'], $title, $s);
              }
            header("Location:index.php");
        } 
        else {
            require_once 'model/model.php';
            $conn = db_get_connection();
            $data = fetchAllCategory($conn);
            session_start();
            if(strlen($_SESSION['userlogin'])==0)
            {
              echo "<script> alert('You must login first!');
                      window.location.href='http://localhost/mob/index.php/login';
                    </script>";
            }
            else{
            include 'views/add.php';
            }
        }
    }
   
    if ($link_array[3] == "post"){
      if ($link_array[4] == "delete"){
        session_start();
        if(strlen($_SESSION['userlogin'])==0)
        {
          echo "<script> alert('You must login first!');
                  window.location.href='http://localhost/mob/index.php/login';
                </script>";
        }
        else{
        require_once 'model/model.php';
        $conn = db_get_connection();
        $id = $link_array[5];
        deleteBlog($id, $conn);
        deleteCategoryRelation($conn, $id);
        header("Location:http://localhost/mob/index.php");
        }}

    if ($link_array[4] == "edit"){       
      require_once 'model/model.php';
      
        session_start();
        if(strlen($_SESSION['userlogin'])==0)
        {
          echo "<script> alert('You must login first!');
                  window.location.href='http://localhost/mob/index.php/login';
                </script>";
        }
        else{

      if (isset($_POST['title'])) {
        $conn = db_get_connection();
        $date = date("Y-m-d H:i:s");
        $title = $_POST['title'];
        $blog = $_POST['content'];
        $tag = $_POST['tags'];
        $id = $link_array[5];
        $words = trimInput($tag);
        $s = sizeof($words);    
        updateBlog($id, $title, $blog, $date, $conn);
        delete($id, $conn);
        for ($i = 0; $i < $s; $i++) {
          $count = tagCount($conn, $words, $i);
          if ($count == 0) {
            insertTag($conn, $words, $i);
          }
            insertRelation($conn, $words, $title, $i);
        }
          deleteTab($conn);
          deleteCategoryRelation($conn, $id); 
          foreach($_POST['list'] as $s) {
            $sql = "SELECT cid FROM CatTable WHERE cname = '$s'";
            $q = $conn->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
            $cid = $q->fetch();
            insertCategoryRelation($conn, $id, $cid['cid'], $title, $s);
          }
          header("Location:http://localhost/mob/index.php");
        } 
        else {
          $conn = db_get_connection();
          $idval = $link_array[5];
          $row = fetchBlog($conn, $idval);
          $data2 = fetchTag($conn, $idval);  
          $data = fetchAllCategory($conn); 
          }
        include 'views/edit.php';
        window.stop();
        }
        }
        
        require_once 'model/model.php';
        $conn = db_get_connection();
        $idval = $link_array[4];
        $row = fetchBlog($conn, $idval);
        $data2 = fetchTag($conn, $idval);
        include 'views/post.php';
      }
      if ($link_array[3]=="list")
      {
      require 'model/model.php';
      $conn = db_get_connection();
      if ($link_array[5] == "pageno"){
        $pageno = $link_array[6];
      } 
      else {
        $pageno = 1;
      }
      if ($link_array[4]=="ASC") {
        $sort = ASC;
      }
       else {
        $sort = DESC;
      }
      $n = 5;
      $offset = ($pageno-1) * $n;
      $total_pages = page($conn, $n);
      $d = sortDisplay($conn, $offset, $sort, $n);
      include 'views/view.php';
    }
    else
    {
      require 'model/model.php';
      $conn = db_get_connection();
      if ($link_array[5] == "pageno"){
        $pageno = $link_array[6];
      } 
      else {
        $pageno = 1;
      }
      if ($link_array[4]=="ASC") {
        $sort = ASC;
      }
       else {
        $sort = DESC;
      }
      $n = 5;
      $offset = ($pageno-1) * $n;
      $total_pages = page($conn, $n);
      $d = sortDisplay($conn, $offset, $sort, $n);
      include 'views/view.php';
    }  
    }
} 
else {
  require 'model/model.php';
  if (isset($_POST['submit'])) {
    $db = $_POST['db'];
    $username = $_POST['username'];
    $password = $_POST['password'];
      if ($username == "root" and $password == "user0123") {
        createFile($username, $password, $db);
        require 'config.php';
        try {
          $conn = new PDO("mysql:host=$hostname", $username, $password);
          $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          dropDb($conn, $dbname);
          createDb($conn, $dbname);
          createTable($conn);
          $dummyNo = $_POST['dummyNo']; 
          while ($dummyNo != 0)  {
            insertDummy($conn);
            $dummyNo--;
            }
        } catch(PDOException $e) {
            $conn->rollback();
            echo "Error: " . $e->getMessage();
          }
          header("Location:index.php");
        } else {
            echo "<script type='text/javascript'> alert('Username or Password is incorrect');</script>";
        }
        }   
        include 'views/install.php';
  }          
?>