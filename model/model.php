<?php
function db_get_connection() { 
  require 'config.php'; 
  static $conn;
  try {
    if (!isset($db)) {
      $conn = new PDO("mysql:host=$hostname;dbname=$dbname", $username, $password);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } 
  } catch(PDOException $ex) {
    echo "An Error occured! " . $ex->getMessage();
  } 
  return $conn; 
}

function trimInput($tag) {
  $tag = str_replace(' ', '', $tag);
  $tag = rtrim($tag, ',');
  $tag = strtolower($tag);
  $words = explode(",", $tag);
  return $words;
}

function trimContent($row) {
  $str = $row["Content"];
  $words = explode(" ", $str);
  $cont = implode(" ", array_splice($words, 0, 200));
  if (str_word_count($cont) > 199) {
    $cont =  $cont."...";
  }
  return $cont;
}

function updateBlog($id, $title, $blog, $date, $conn) {
  $sqq = "UPDATE BlogDetails SET Title = :ti, Content = :bl, Date = '$date' WHERE id = '$id';";
  $ps = $conn->prepare($sqq);
  $ps->bindValue(":ti", $title);
  $ps->bindValue(":bl", $blog);
  $ps->execute();
}

function updateCategory($cid, $cname, $conn) {
  $sql = "UPDATE CatTable SET cname = '$cname' WHERE cid = '$cid';";
  $conn->exec($sql);
  header("location:http://localhost/mob/index.php/category");

}

function insertBlog($title, $blog, $date, $conn) {
  $sq = "INSERT INTO BlogDetails (Title, Content, Date) VALUES (:ti, :bl, '$date');";
  $ps = $conn->prepare($sq);
  $ps->bindValue(":ti", $title);
  $ps->bindValue(":bl", $blog);
  $ps->execute();
}

function delete($id, $conn) {
  $sql = "DELETE FROM Reltab WHERE id = '$id'";
  $conn->exec($sql);
}

function deleteBlog($id, $conn) {
  $sql = "DELETE FROM BlogDetails WHERE id = '$id';";
  $conn->exec($sql);
  $sql = "DELETE FROM Reltab WHERE id = '$id';";
  $conn->exec($sql);
  $sql = "DELETE FROM CatRel WHERE id = '$id';";
  $conn->exec($sql);
  $sq = "DELETE FROM TagTable WHERE tid NOT IN (SELECT tid FROM Reltab)";
  $conn->exec($sq);
  $sq = "DELETE FROM CatTable WHERE cid NOT IN (SELECT cid FROM CatRel)";
  $conn->exec($sq);  
}

function tagCount($conn, $words, $i) {
  $tagrl = "SELECT tname FROM TagTable WHERE  tname = '$words[$i]';";
  $res = $conn->query($tagrl); 
  $res->setFetchMode(PDO::FETCH_ASSOC);
  $count = $res->rowCount();
  return $count;
}

function categoryCount($conn, $cat) {
  $tagrl = "SELECT cname FROM CatTable WHERE  cname = '$cat';";
  $res = $conn->query($tagrl);
  $res->setFetchMode(PDO::FETCH_ASSOC);
  $count = $res->rowCount();
  return $count;
}

function insertTag($conn, $words, $i) {
  $sq = "INSERT INTO TagTable (tname) VALUES ('$words[$i]');";
  $conn->exec($sq);
}

function insertCategory($conn, $cat) {
  $sq = "INSERT INTO CatTable (cname) VALUES ('$cat');";
  $conn->exec($sq);
}

function insertRelation($conn, $words, $title, $i) {
  $sq = "INSERT INTO Reltab (id, tid) SELECT bd.id, tt.tid FROM BlogDetails bd JOIN TagTable tt ON bd.Title = '$title' AND tt.tname = '$words[$i]';";
  $conn->exec($sq);
}

function insertCategoryRelation($conn, $id, $cid, $a1, $a4) {
  $sq = "INSERT INTO CatRel (id, cid) SELECT bd.id, ct.cid FROM BlogDetails bd JOIN CatTable ct ON bd.Title = '$a1' AND ct.cname = '$a4'";
  $conn->exec($sq);
}

function deleteTab($conn) {
  $sq = "DELETE FROM TagTable WHERE tid NOT IN (SELECT tid FROM Reltab)";
  $conn->exec($sq);
}

function deleteCategory($conn, $id) {
  $sq = "SELECT id from CatRel WHERE cid = '$id'";
  $res = $conn->query($sq);
  $res->setFetchMode(PDO::FETCH_ASSOC);
  $count = $res->rowCount();
  if ($count == 0) {
    $sq = "DELETE FROM CatTable WHERE cid = '$id'";
    $conn->exec($sq);
  } else {
    echo "<script> alert('Cannot be deleted! This category is used by an existing Blog');
    window.location.href='http://localhost/mob/index.php/category';
    </script>";
  }
}

function deleteCategoryRelation($conn, $id) {
  $sql = "DELETE FROM CatRel WHERE id = '$id';";
  $conn->exec($sql);
  echo asd;
}

function fetchBlog($conn, $idval) {
  $sql = "SELECT id, Title, Content, Date FROM BlogDetails WHERE id = ?";
  $s = $conn->prepare($sql);
  $s->execute([$idval]);
  $row = $s->fetch();
  return $row;
}

function fetchTag($conn, $idval) {
  $sql1 = "SELECT TagTable.tname, TagTable.tid FROM Reltab, TagTable WHERE Reltab.id = ? AND TagTable.tid = Reltab.tid";
  $s2 = $conn->prepare($sql1);
  $s2->execute([$idval]);
  $data2 = $s2-> fetchAll();
  return $data2;
}

function fetchCategory($conn, $val) {
  $s = "SELECT CatTable.cname, CatTable.cid FROM CatRel, CatTable WHERE CatRel.id = ? AND CatTable.cid = CatRel.cid";
  $res = $conn->prepare($s);
  $res->execute([$val]);
  $dat = $res-> fetchAll();
  return $dat;
}

function fetchAllCategory($conn) {
  $s = "SELECT cid, cname FROM CatTable";
  $res = $conn->prepare($s);
  $res->execute();
  $dat = $res-> fetchAll();
  return $dat;
}


function createFile($username,$password,$db) {
  $file = fopen("config.php", "w") or die("Unable to Open");
  $wr = '<?php
  $hostname = "localhost";
  $username = "'.$username.'";
  $password = "'.$password.'";
  $dbname = "'.$db.'"; 
  ?>';
  fwrite($file, $wr);
  fclose($file);
}

function dropDb($conn, $dbname) {
  $sql = "DROP DATABASE IF EXISTS ".$dbname.";";
  $conn->exec($sql);
}

function createDb($conn, $dbname) {
  $sql = "CREATE DATABASE ".$dbname.";";
  $conn->exec($sql);
  $conn->exec("USE ".$dbname.";");
}

function createTable($conn) {
  $table1 = "CREATE TABLE BlogDetails (
    id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    Title  varchar(50) NOT NULL,
    Content text NOT NULL,
    Date  date NOT NULL)";
  $conn->exec($table1);
  $table7 = "CREATE TABLE users (
    id int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username  varchar(50) NOT NULL,
    password  varchar(50) NOT NULL,
    email  varchar(50) NOT NULL)";
  $conn->exec($table7);
  $table2 = "CREATE TABLE TagTable (
      tid  int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
      tname varchar(100) NOT NULL) ";
  $conn->exec($table2);
  $table3 = "CREATE TABLE Reltab (
    id  int(11) NOT NULL,
    tid int(11) NOT NULL)";
  $conn->exec($table3);
  $table4 = "CREATE TABLE CatTable (
    cid  int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    cname varchar(100) NOT NULL) ";
  $conn->exec($table4);
  $table5 = "CREATE TABLE CatRel (
    id  int(11) NOT NULL,
    cid int(11) NOT NULL)";
  $conn->exec($table5);

}

function genDummy($n) {
  $b = array(  'lorem', 'ipsum', 'dolor', 'sit', 'amet', 'consectetur', 'adipiscing', 'elit',
        'a', 'ac', 'accumsan', 'ad', 'aenean', 'aliquam', 'aliquet', 'ante',
        'aptent', 'arcu', 'at', 'auctor', 'augue', 'bibendum', 'blandit',
        'class', 'commodo', 'condimentum', 'congue', 'consequat', 'conubia',
        'convallis', 'cras', 'cubilia', 'curabitur', 'curae', 'cursus',
        'dapibus', 'diam', 'dictum', 'dictumst', 'dignissim', 'dis', 'donec',
        'dui', 'duis', 'efficitur', 'egestas', 'eget', 'eleifend', 'elementum',
        'enim', 'erat', 'eros', 'est', 'et', 'etiam', 'eu', 'euismod', 'ex',
        'facilisi', 'facilisis', 'fames', 'faucibus', 'felis', 'fermentum',
        'feugiat', 'finibus', 'fringilla', 'fusce', 'gravida', 'habitant',
        'habitasse', 'hac', 'hendrerit', 'himenaeos', 'iaculis', 'id',
        'imperdiet', 'in', 'inceptos', 'integer', 'interdum', 'justo',
        'lacinia', 'lacus', 'laoreet', 'lectus', 'leo', 'libero', 'ligula',
        'litora', 'lobortis', 'luctus', 'maecenas', 'magna', 'magnis',
        'malesuada', 'massa', 'mattis', 'mauris', 'maximus', 'metus', 'mi',
        'molestie', 'mollis', 'montes', 'morbi', 'mus', 'nam', 'nascetur',
        'natoque', 'nec', 'neque', 'netus', 'nibh', 'nisi', 'nisl', 'non',
        'nostra', 'nulla', 'nullam', 'nunc', 'odio', 'orci', 'ornare',
        'parturient', 'pellentesque', 'penatibus', 'per', 'pharetra',
        'phasellus', 'placerat', 'platea', 'porta', 'porttitor', 'posuere',
        'potenti', 'praesent', 'pretium', 'primis', 'proin', 'pulvinar',
        'purus', 'quam', 'quis', 'quisque', 'rhoncus', 'ridiculus', 'risus',
        'rutrum', 'sagittis', 'sapien', 'scelerisque', 'sed', 'sem', 'semper',
        'senectus', 'sociosqu', 'sodales', 'sollicitudin', 'suscipit',
        'suspendisse', 'taciti', 'tellus', 'tempor', 'tempus', 'tincidunt',
        'torquent', 'tortor', 'tristique', 'turpis', 'ullamcorper', 'ultrices',
        'ultricies', 'urna', 'ut', 'varius', 'vehicula', 'vel', 'velit',
        'venenatis', 'vestibulum', 'vitae', 'vivamus', 'viverra', 'volutpat',
        'vulputate',
  );
  $rand = array(); 
  shuffle($b);
  for ($i = 0; $i < $n; $i++) {
    $rand[$i] = $b[$i];
  }
  $rand = implode(" ", $rand);
  return $rand;
}

function insertDummy($conn) {
  $date = date("Y-m-d H:i:s");
  $a1 = genDummy(4);
  $a2 = genDummy(30);
  $a3 = genDummy(1);
  $a4 = genDummy(1);
  $sql = "INSERT INTO BlogDetails (Title, Content, Date) VALUES ('$a1', '$a2', '$date');";
  $conn->exec($sql);
  $sql = "INSERT INTO TagTable (tname) VALUES('$a3');";
  $conn->exec($sql);
  $sql = "INSERT INTO Reltab (id, tid) SELECT bd.id, tt.tid FROM BlogDetails bd JOIN TagTable tt ON bd.Title = '$a1' AND tt.tname = '$a3'";
  $conn->exec($sql);
  $sql = "INSERT INTO CatTable (cname) VALUES('$a4');";
  $conn->exec($sql);
  $sql = "INSERT INTO CatRel (id, cid) SELECT bd.id, ct.cid FROM BlogDetails bd JOIN CatTable ct ON bd.Title = '$a1' AND ct.cname = '$a4'";
  $conn->exec($sql);
}

function sortDisplay($conn, $offset, $sort, $n) {
  $a = ASC;
  if ($sort == $a) {
    $q = "SELECT id, Title, Content, Date FROM BlogDetails ORDER BY id ASC LIMIT $offset, $n";
  } else {
    $q = "SELECT id, Title, Content, Date FROM BlogDetails ORDER BY id DESC LIMIT $offset, $n";
  }
  $s = $conn->prepare($q);
  $s->execute();
  $d = $s->fetchAll();
  return $d;
}

function x($d) {
  $arr =[];
  foreach ($d as $row) {
    $val = $row["id"];
    $val1 = $row["Title"];
    $cont = trimContent($row);
    $val2 = $row["Date"];
    array_push($arr, $val, $val1, $cont, $val2);
  }
 return $arr;
}

function y($a,$conn) {
  foreach ($a as $row) {
  $val = $row["id"];
  $sql1 = "SELECT TagTable.tname, TagTable.tid FROM Reltab, TagTable WHERE Reltab.id = ? AND TagTable.tid = Reltab.tid";
  $s2 = $conn->prepare($sql1);
  $s2->execute([$val]);
  $dat = $s2-> fetchAll();
  $arrr =[];
  foreach ($dat as $row2) {
    $tagidval = $row2["tid"];
    $tags = $row2["tname"];
    array_push($arrr, $tagidval, $tags);
   }}
   return $arrr;
}


function sortDisplaycategory($conn, $offset, $sort, $n, $catIdValue) {
  $a = ASC;
  if ($sort == $a) {
    $q = "SELECT BlogDetails.id, BlogDetails.Title, BlogDetails.Content, BlogDetails.Date FROM BlogDetails, CatRel WHERE BlogDetails.id = CatRel.id AND CatRel.id = ?  ORDER BY BlogDetails.id ASC LIMIT $offset,$n";
  } else {
    $q = "SELECT BlogDetails.id, BlogDetails.Title, BlogDetails.Content, BlogDetails.Date FROM BlogDetails, CatRel WHERE BlogDetails.id = CatRel.id AND CatRel.cid = ? ORDER BY BlogDetails.id DESC LIMIT $offset,$n";
  }
  $s = $conn->prepare($q);
  $s->execute([$catIdValue]);
  $d = $s->fetchAll();
  return $d;
}


function sortDisplayTag($conn, $offset, $sort, $n, $tagIdValue) {
  $a = ASC;
  if ($sort == $a) {
    $q = "SELECT BlogDetails.id, BlogDetails.Title, BlogDetails.Content, BlogDetails.Date FROM BlogDetails, Reltab WHERE BlogDetails.id = Reltab.id AND Reltab.tid = ?  ORDER BY BlogDetails.id ASC LIMIT $offset,$n";
  } else {
    $q = "SELECT BlogDetails.id, BlogDetails.Title, BlogDetails.Content, BlogDetails.Date FROM BlogDetails, Reltab WHERE BlogDetails.id = Reltab.id AND Reltab.tid = ? ORDER BY BlogDetails.id DESC LIMIT $offset,$n";
  }
  $s = $conn->prepare($q);
  $s->execute([$tagIdValue]);
  $d = $s->fetchAll();
  return $d;
}

function page($conn, $n) {
  $total_pages_sql = "SELECT id FROM BlogDetails";
  $q1 = $conn->query($total_pages_sql);
  $total_rows = $q1->rowCount();
  $total_pages = ceil($total_rows / $n);
  return $total_pages;
}

function pageTag($conn, $n, $tagIdValue) {
  $total_pages_sql = "SELECT BlogDetails.id, BlogDetails.Title, BlogDetails.Content, BlogDetails.Date FROM BlogDetails, Reltab WHERE BlogDetails.id = Reltab.id AND Reltab.tid = '$tagIdValue';";
  $q1 = $conn->query($total_pages_sql);
  $total_rows = $q1->rowCount();
  $total_pages = ceil($total_rows / $n);
  return $total_pages;
}
?>