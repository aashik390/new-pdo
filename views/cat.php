<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Test Blog</title>
  <!-- Bootstrap core CSS -->
  <link href="http://localhost/mob/views/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="http://localhost/mob/views/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- Custom styles for this template -->
  <link href="http://localhost/mob/views/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/mob/index.php">Home</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('http://localhost/mob/views/img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Category Management</h1>  
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="dropdown">
          <?php
          if (isset($d)) {
            foreach ($d as $row) {
              $id = $row['cid'];
              echo '<div class="post-preview"> 
              <h4 class="post-subtitle">';
              echo '<a href="http://localhost/mob/index.php/category/show/'.$id.'">';
              echo $row["cname"]." ".'</h4>';
              echo '<a href="http://localhost/mob/index.php/category/edit/';  echo $id; echo '"> Edit </a>';
              echo "<a href=\"http://localhost/mob/index.php/category/$id\" onclick=\"return confirm('Are you sure you want to delete this blog?');\"> Delete </a>";
              echo "</div> <hr>";
            }
          }
          ?>
        </div>
        <form name="blogform" action="http://localhost/mob/index.php/category" method="POST">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <input type="text" class="form-control" placeholder="Add new Category here" name="cat" required data-validation-required-message="Please enter the data.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name= "submit" id="AddBlogButton" >Add</button>
          </div>
        </form>  
      </div>
    </div>
  </div>
  <hr>
  <!-- Bootstrap core JavaScript -->
  <script src="http://localhost/mob/views/js/jquery.min.js"></script>
  <script src="http://localhost/mob/views/js/bootstrap.bundle.min.js"></script>
  <!-- Custom scripts for this template -->
  <script src="http://localhost/mob/views/js/clean-blog.min.js"></script>
</body>

</html>