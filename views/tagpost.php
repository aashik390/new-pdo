<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Test Blog</title>
  <!-- Bootstrap core CSS -->
  <link href="http://localhost/mob/views/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="http://localhost/mob/views/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- Custom styles for this template -->
  <link href="http://localhost/mob/views/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/mob/index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/mob/index.php/add">Add Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('http://localhost/mob/views/img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Tag Posts</h1>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">              
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort by
          <span class="caret"></span></button>
          <ul class="dropdown-menu">  
            <li><a href="http://localhost/mob/index.php/tag/<?php echo $link_array[4]; ?>/ASC/<?php echo $pageno; ?>">Ascending</a></li>
            <li><a href="http://localhost/mob/index.php/tag/<?php echo $link_array[4]; ?>/DESC/<?php echo $pageno; ?>">Descending</a></li>
          </ul>
        </div>
        <?php
        if (isset($d)) {
          foreach ($d as $row) {
            $val = $row["id"];
            $cont = trimContent($row);
            echo '<div class="post-preview"> 
            <a href="http://localhost/mob/index.php/post/'.$val.'">
              <h2 class="post-title">'.$row["Title"].'</h2>
              <h3 class="post-subtitle">'.$cont.'</h3>
              </a><p class="post-meta">Posted on '.$row["Date"].'</p>  ';
            $data2 = fetchTag($conn, $val);
            echo "<p>Tags: ";
            if (isset($data2)) { 
              foreach ($data2 as $row2) {
              $tagidval = $row2["tid"];
              echo '<a href="http://localhost/mob/index.php/tag/'.$tagidval.'">'.$row2["tname"].' </a>';      
            }
            echo "</p>";
            }
            echo "<p>Categories: ";
            $dat = fetchCategory($conn, $val);
            if (isset($dat)) {
              foreach ($dat as $row2) {
                $tagidval = $row2["cid"];
                echo '<a href="http://localhost/mob/index.php/category/show/'.$tagidval.'">';
                echo $row2["cname"]." ";
            }
            echo "</p>
            </div>
            <hr>";
          }
            
          }  
        } else {
          echo "0 results";
        }
        ?>

        <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if ($pageno == 1) { echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if ($pageno == 1) { echo '#'; } else { echo "http://localhost/mob/index.php/tag/$tagIdValue/sort/$sort/pageno/".($pageno - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if ($pageno == $total_pages) { echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if ($pageno == $total_pages) { echo '#'; } else { echo "http://localhost/mob/index.php/tag/$tagIdValue/sort/$sort/pageno/".($pageno + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>  
  <!-- Bootstrap core JavaScript -->
  <script src="http://localhost/mob/views/js/jquery.min.js"></script>
  <script src="http://localhost/mob/views/js/bootstrap.bundle.min.js"></script>
  <!-- Custom scripts for this template -->
  <script src="http://localhost/mob/views/js/clean-blog.min.js"></script>
</body>

</html>