<?php
session_start();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Test Blog</title>
  <!-- Bootstrap core CSS -->
  <link href="http://localhost/mob/views/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="http://localhost/mob/views/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- Custom styles for this template -->
  <link href="http://localhost/mob/views/css/clean-blog.min.css" rel="stylesheet">
</head>

<body>
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <?php
            session_start();
            if(strlen($_SESSION['userlogin'])==0)
            { ?>
              <li class="nav-item">
              <a class="nav-link" href="http://localhost/mob/index.php/login">Log-in</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="http://localhost/mob/index.php/signup">Register</a>
          </li>
           <?php }
            else{ ?>
              <li class="nav-item">
                        <a class="nav-link" href="http://localhost/mob/index.php/logout">Log-out</a>
                      </li>
           <?php }
          ?> 
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/mob/index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/mob/index.php/category">Categories</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/mob/index.php/add">Add Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('http://localhost/mob/views/img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Make this your chance!</h1>
            <?php
              if(strlen($_SESSION['userlogin'])!==0)
              {
                      $username=$_SESSION['userlogin'];
                      $query=$conn->prepare("SELECT  username FROM users WHERE (username=:username)");
                      $query->execute(array(':username'=> $username));
                      while($row=$query->fetch(PDO::FETCH_ASSOC)){
                      $username=$row['username'];
                    }
                    echo  'Welcome '.$username;    
                    }
                    ?>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
      <div class="dropdown">
        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort by
        <span class="caret"></span></button>
        <ul class="dropdown-menu">  
          <li><a href="http://localhost/mob/index.php/sort/ASC/pageno/<?php echo $pageno; ?>">Ascending</a></li>
          <li><a href="http://localhost/mob/index.php/sort/DESC/pageno/<?php echo $pageno; ?>">Descending</a></li>
        </ul>
      </div>
      <?php
      // $array = x($d);
      // $a = count($array) / 4;
      // for ($i=0;$i<=20;$i=$i+4)
      // {
      //   print_r ($array[$i+1]);
      //   echo "<br><br>";
      //   print_r ($array[$i+2]);
      //   echo "<br><br>";
      //   print_r ($array[$i+3]);
      //   echo "<br><br>";
      //   $arrr= y($a , $conn);
      //   print_r ($arrr[1]);
      //   echo "<hr>";
      // }
      if (isset($d)) {
        foreach ($d as $row) {
          $val = $row["id"];
          $cont = trimContent($row);
          echo '<div class="post-preview"> 
            <a href="index.php/post/'.$val.'">
            <h2 class="post-title">'.$row["Title"].'</h2>
            <h5 class="post-subtitle">'.$cont.'</h5>
            </a><p class="post-meta">Posted on '.$row["Date"].'</p>';
          echo "<p>Tags: ";
          $dat = fetchTag($conn, $val);
          if (isset($dat)) {
            foreach ($dat as $row2) {
              $tagidval = $row2["tid"];
              echo '<a href="http://localhost/mob/index.php/tag/'.$tagidval.'">'.$row2["tname"].' </a>';
            }
          echo "</p>";
          }
          echo "<p>Categories: ";
          $dat = fetchCategory($conn, $val);
          if (isset($dat)) {
            foreach ($dat as $row2) {
              $tagidval = $row2["cid"];
             echo '<a href="http://localhost/mob/index.php/category/show/'.$tagidval.'">';
              echo $row2["cname"]." ";
            }
          echo "</p>"; 
          }
          echo "</div>
          <hr>";
        }
      }
      ?>
<!-- Pager -->
        <div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if ($pageno == 1) { echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if ($pageno == 1) { echo '#'; } else { echo "http://localhost/mob/index.php/sort/$sort/pageno/".($pageno - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if ($pageno == $total_pages) { echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if ($pageno == $total_pages) { echo '#'; } else { echo "http://localhost/mob/index.php/sort/$sort/pageno/".($pageno + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <!-- Bootstrap core JavaScript -->
  <script src="http://localhost/mob/views/js/jquery.min.js"></script>
  <script src="http://localhost/mob/views/js/bootstrap.bundle.min.js"></script>
  <!-- Custom scripts for this template -->
  <script src="http://localhost/mob/views/js/clean-blog.min.js"></script>
</body>

</html>
